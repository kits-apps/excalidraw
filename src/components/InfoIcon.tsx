import { info } from "../components/icons";

type InfoIconProps = {
  title?: string;
  name?: string;
  id?: string;
  onClick?(): void;
};

export const InfoIcon = (props: InfoIconProps) => (
  <button
    className="info-icon"
    onClick={props.onClick}
    type="button"
    title={`${props.title} — ?`}
    aria-label={props.title}
  >
    {info}
  </button>
);
